let arr = ["A", "B", "C"];

function reverseArray(arr) {
    let reverse = [];
    for (let i = arr.length - 1; i >= 0; i--) {
        reverse.push(arr[i])
    }
    return reverse;
}

let arr2 = [1, 2, 3, 4, 5]
function reverseArrayInPlace(arr2) {
    //swap
    for (let i = 0; i <= (arr2.length / 2); i++) {
        let temp = arr2[i];
        arr2[i] = arr2[arr2.length - 1 - i];
        arr2[arr2.length - 1 - i] = temp;
    }
    return arr2;
}